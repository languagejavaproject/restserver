package cs.vsu.javaLanguage.RESTServer.redis.repository;

import cs.vsu.javaLanguage.RESTServer.redis.model.Employee;
import org.springframework.data.keyvalue.repository.KeyValueRepository;

import java.util.Optional;

public interface EmployeeSalaryRedisRepository extends KeyValueRepository<Employee, Integer> {

    Optional<Employee> findByEmployeeId(String employeeId);

}
