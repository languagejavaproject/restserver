package cs.vsu.javaLanguage.RESTServer.redis.model;

import org.springframework.data.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;
import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("Employee")
public class Employee implements Serializable {

    @Id
    private String employeeId;
    private double daySalary;

}
