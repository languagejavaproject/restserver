package cs.vsu.javaLanguage.RESTServer.postgres.repository;

import cs.vsu.javaLanguage.RESTServer.postgres.model.EmployeeSalary;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface EmployeeSalaryPostgresRepository extends JpaRepository<EmployeeSalary, Integer> {

    Optional<EmployeeSalary> findByEmployeeIdAndDate(Long employeeId, LocalDate date);

}
