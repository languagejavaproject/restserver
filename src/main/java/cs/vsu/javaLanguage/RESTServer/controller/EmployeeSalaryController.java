package cs.vsu.javaLanguage.RESTServer.controller;

import cs.vsu.javaLanguage.RESTServer.dto.EmployeeDTO;
import cs.vsu.javaLanguage.RESTServer.service.EmployeeSalaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/api/v1/employee")
@RequiredArgsConstructor
public class EmployeeSalaryController {

    private final EmployeeSalaryService employeeSalaryService;

    @PostMapping("/salary")
    public ResponseEntity<?> getEmployeeSalary(@RequestBody EmployeeDTO employeeDTO) {
        EmployeeDTO employeeSalary = employeeSalaryService.findSalaryByDate(employeeDTO);
        return ResponseEntity.ok(employeeSalary);
    }

}
