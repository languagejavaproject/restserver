package cs.vsu.javaLanguage.RESTServer.service;

import cs.vsu.javaLanguage.RESTServer.dto.EmployeeDTO;
import cs.vsu.javaLanguage.RESTServer.redis.model.Employee;
import cs.vsu.javaLanguage.RESTServer.postgres.model.EmployeeSalary;
import cs.vsu.javaLanguage.RESTServer.postgres.repository.EmployeeSalaryPostgresRepository;
import cs.vsu.javaLanguage.RESTServer.redis.repository.EmployeeSalaryRedisRepository;
import cs.vsu.javaLanguage.RESTServer.mapper.EmployeeMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeeSalaryService {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeSalaryService.class);

    private final EmployeeSalaryPostgresRepository employeeSalaryPostgresRepository;

    private final EmployeeSalaryRedisRepository employeeSalaryRedisRepository;

    private final EmployeeMapper employeeMapper;

    public EmployeeDTO findSalaryByDate(EmployeeDTO employeeDTO) throws IllegalArgumentException {
        String employeeId = employeeDTO.getEmployeeId();
        LocalDate employeeDate = employeeDTO.getDate();

        if (Integer.parseInt(employeeId) <= 0) {
            logger.warn("Invalid employee ID: {}", employeeId);
            throw new IllegalArgumentException("Invalid employee ID");
        }

        if (employeeDate == null || Objects.equals(employeeDate, LocalDate.now())) {
            return findTodaySalaryById(employeeDTO);
        }

        return findByDateAndId(employeeDTO);
    }

    public EmployeeDTO findByDateAndId(EmployeeDTO employeeDTO) throws IllegalArgumentException {
        Long employeeId = Long.valueOf(employeeDTO.getEmployeeId());
        LocalDate employeeDate = employeeDTO.getDate();
        Optional<EmployeeSalary> employeeSalary = employeeSalaryPostgresRepository.findByEmployeeIdAndDate(employeeId, employeeDate);

        if (employeeSalary.isEmpty()) {
            logger.warn("Employee salary not found for given date and ID: {}, {}", employeeDate, employeeId);
            throw new IllegalArgumentException("Employee salary not found for given date and ID");
        }

        EmployeeSalary employeeResponse = employeeSalary.get();
        return employeeMapper.toDTO(employeeResponse);
    }

    public EmployeeDTO findTodaySalaryById(EmployeeDTO employeeDTO) throws IllegalArgumentException {
        String employeeId = employeeDTO.getEmployeeId();
        Optional<Employee> employeeSalary = employeeSalaryRedisRepository.findByEmployeeId(employeeId);

        if (employeeSalary.isEmpty()) {
            logger.warn("Employee salary not found for given ID: {}", employeeId);
            throw new IllegalArgumentException("Employee salary not found for given ID");
        }

        Employee employeeResponse = employeeSalary.get();
        return employeeMapper.toDTO(employeeResponse);
    }

}
