package cs.vsu.javaLanguage.RESTServer.mapper;

import cs.vsu.javaLanguage.RESTServer.dto.EmployeeDTO;
import cs.vsu.javaLanguage.RESTServer.postgres.model.EmployeeSalary;
import cs.vsu.javaLanguage.RESTServer.redis.model.Employee;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class EmployeeMapper {

    public EmployeeDTO toDTO(EmployeeSalary employeeResponse) {
        return EmployeeDTO.builder()
                .employeeId(String.valueOf(employeeResponse.getEmployeeId()))
                .salary(employeeResponse.getSalary())
                .date(employeeResponse.getDate())
                .build();
    }

    public EmployeeDTO toDTO(Employee employeeResponse) {
        return EmployeeDTO.builder()
                .employeeId(employeeResponse.getEmployeeId())
                .salary(employeeResponse.getDaySalary())
                .date(LocalDate.now())
                .build();
    }

}
