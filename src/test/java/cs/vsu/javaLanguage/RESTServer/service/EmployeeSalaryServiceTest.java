package cs.vsu.javaLanguage.RESTServer.service;

import cs.vsu.javaLanguage.RESTServer.dto.EmployeeDTO;
import cs.vsu.javaLanguage.RESTServer.postgres.model.EmployeeSalary;
import cs.vsu.javaLanguage.RESTServer.postgres.repository.EmployeeSalaryPostgresRepository;
import cs.vsu.javaLanguage.RESTServer.redis.model.Employee;
import cs.vsu.javaLanguage.RESTServer.redis.repository.EmployeeSalaryRedisRepository;
import cs.vsu.javaLanguage.RESTServer.mapper.EmployeeMapper;
import cs.vsu.javaLanguage.RESTServer.service.EmployeeSalaryService;
import static org.mockito.BDDMockito.given;
import static org.mockito.ArgumentMatchers.anyString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class EmployeeSalaryServiceTest {

    @Mock
    private EmployeeSalaryPostgresRepository employeeSalaryPostgresRepository;
    @Mock
    private EmployeeSalaryRedisRepository employeeSalaryRedisRepository;
    @Mock
    private EmployeeMapper employeeMapper;
    @InjectMocks
    private EmployeeSalaryService employeeSalaryService;

    @Test
    void findSalaryByDate_shouldReturnEmployeeDTO_whenEmployeeIdAndDateAreValid() throws IllegalArgumentException {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .employeeId("1")
                .date(LocalDate.of(2023, 2, 3))
                .build();
        EmployeeSalary employeeSalary = EmployeeSalary.builder()
                .employeeId(1)
                .salary(1000.0)
                .date(LocalDate.of(2023, 2, 3))
                .build();
        EmployeeDTO expectedEmployeeDTO = EmployeeDTO.builder()
                .employeeId("1")
                .salary(1000.0)
                .date(LocalDate.of(2023, 2, 3))
                .build();

        BDDMockito.given(employeeSalaryPostgresRepository.findByEmployeeIdAndDate(ArgumentMatchers.anyLong(), ArgumentMatchers.any(LocalDate.class)))
                .willReturn(Optional.of(employeeSalary));
        BDDMockito.given(employeeMapper.toDTO(ArgumentMatchers.any(EmployeeSalary.class)))
                .willReturn(expectedEmployeeDTO);

        EmployeeDTO actualEmployeeDTO = employeeSalaryService.findSalaryByDate(employeeDTO);

        Assertions.assertEquals(expectedEmployeeDTO, actualEmployeeDTO);
    }

    @Test
    void findSalaryByDate_shouldThrowIllegalArgumentException_whenEmployeeIdIsInvalid() {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .employeeId("0")
                .date(LocalDate.of(2023, 2, 3))
                .build();

        Assertions.assertThrows(IllegalArgumentException.class, () -> employeeSalaryService.findSalaryByDate(employeeDTO));
    }

    @Test
    void findSalaryByDate_shouldThrowIllegalArgumentException_whenEmployeeDateIsInvalid() {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .employeeId("1")
                .date(null)
                .build();

        Assertions.assertThrows(IllegalArgumentException.class, () -> employeeSalaryService.findSalaryByDate(employeeDTO));
    }

    @Test
    void findSalaryByDate_shouldThrowIllegalArgumentException_whenEmployeeSalaryNotFound() {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .employeeId("1")
                .date(LocalDate.of(2023, 2, 3))
                .build();

        BDDMockito.given(employeeSalaryPostgresRepository.findByEmployeeIdAndDate(ArgumentMatchers.anyLong(), ArgumentMatchers.any(LocalDate.class)))
                .willReturn(Optional.empty());

        Assertions.assertThrows(IllegalArgumentException.class, () -> employeeSalaryService.findSalaryByDate(employeeDTO));
    }

    @Test
    void findTodaySalaryById_shouldReturnEmployeeDTO_whenEmployeeIdIsValid() throws IllegalArgumentException {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .employeeId("1")
                .date(LocalDate.now())
                .build();
        Employee employee = Employee.builder()
                .employeeId("1")
                .daySalary(1000.0)
                .build();
        EmployeeDTO expectedEmployeeDTO = EmployeeDTO.builder()
                .employeeId("1")
                .salary(1000.0)
                .build();

        expectedEmployeeDTO.setDate(LocalDate.now());
        BDDMockito.given(employeeSalaryRedisRepository.findByEmployeeId(ArgumentMatchers.anyString()))
                .willReturn(Optional.of(employee));
        BDDMockito.given(employeeMapper.toDTO(ArgumentMatchers.any(Employee.class)))
                .willReturn(expectedEmployeeDTO);

        EmployeeDTO actualEmployeeDTO = employeeSalaryService.findTodaySalaryById(employeeDTO);

        Assertions.assertEquals(expectedEmployeeDTO, actualEmployeeDTO);
    }

    @Test
    void findTodaySalaryById_shouldThrowIllegalArgumentException_whenEmployeeIdIsInvalid() {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .employeeId("0")
                .date(LocalDate.now())
                .build();

        Assertions.assertThrows(IllegalArgumentException.class, () -> employeeSalaryService.findTodaySalaryById(employeeDTO));
    }

    @Test
    void findTodaySalaryById_shouldThrowIllegalArgumentException_whenEmployeeSalaryNotFound() {
        EmployeeDTO employeeDTO = EmployeeDTO.builder()
                .employeeId("1")
                .date(LocalDate.now())
                .build();

        given(employeeSalaryRedisRepository.findByEmployeeId(anyString())).willReturn(Optional.empty());

        Assertions.assertThrows(IllegalArgumentException.class, () -> employeeSalaryService.findTodaySalaryById(employeeDTO));
    }
}
