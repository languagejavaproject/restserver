package cs.vsu.javaLanguage.RESTServer.redis.repository;

import cs.vsu.javaLanguage.RESTServer.redis.model.Employee;
import cs.vsu.javaLanguage.RESTServer.redis.repository.EmployeeSalaryRedisRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.redis.DataRedisTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataRedisTest
@ActiveProfiles("test")
class EmployeeSalaryRedisRepositoryUnitTest {

    @Autowired
    private EmployeeSalaryRedisRepository employeeRedisRepository;

    @AfterEach
    void tearDown() {
        employeeRedisRepository.deleteAll();
    }

    @Test
    void testSaveEmployee() {
        Employee employee = Employee.builder()
                .employeeId("1")
                .daySalary(1000.0)
                .build();

        Employee savedEmployee = employeeRedisRepository.save(employee);

        assertThat(savedEmployee).isNotNull();
        assertThat(savedEmployee.getDaySalary()).isEqualTo(1000.0);
        assertThat(savedEmployee.getEmployeeId()).isEqualTo("1");
    }

    @Test
    void testFindAllEmployees() {
        Employee employee1 = Employee.builder()
                .employeeId("1")
                .daySalary(1000.0)
                .build();

        Employee employee2 = Employee.builder()
                .employeeId("2")
                .daySalary(2000.0)
                .build();

        employeeRedisRepository.save(employee1);
        employeeRedisRepository.save(employee2);

        List<Employee> employees = employeeRedisRepository.findAll();

        assertThat(employees).hasSize(2);
        assertThat(employees).contains(employee1, employee2);
    }

    @Test
    void testUpdateEmployee() {
        Employee employee = Employee.builder()
                .employeeId("1")
                .daySalary(1000.0)
                .build();

        Employee savedEmployee = employeeRedisRepository.save(employee);

        savedEmployee.setDaySalary(2000.0);

        Employee updatedEmployee = employeeRedisRepository.save(savedEmployee);

        assertThat(updatedEmployee).isNotNull();
        assertThat(updatedEmployee.getDaySalary()).isEqualTo(2000.0);
        assertThat(updatedEmployee.getEmployeeId()).isEqualTo("1");
    }

    @Test
    void testDeleteEmployee() {
        Employee employee = Employee.builder()
                .employeeId("1")
                .daySalary(1000.0)
                .build();

        employeeRedisRepository.save(employee);

        employeeRedisRepository.deleteById(Integer.valueOf(employee.getEmployeeId()));

        Optional<Employee> deletedEmployee = employeeRedisRepository.findById(Integer.valueOf(employee.getEmployeeId()));

        assertThat(deletedEmployee).isEmpty();
    }

    @Test
    void testFindByEmployeeId() {
        Employee employee = Employee.builder()
                .employeeId("1")
                .daySalary(1000.0)
                .build();

        Employee savedEmployee = employeeRedisRepository.save(employee);

        String generatedEmployeeId = savedEmployee.getEmployeeId();

        Optional<Employee> foundEmployee = employeeRedisRepository.findById(Integer.valueOf(generatedEmployeeId));

        assertThat(foundEmployee).isPresent();
        assertThat(foundEmployee.get().getEmployeeId()).isEqualTo(generatedEmployeeId);
        assertThat(foundEmployee.get().getDaySalary()).isEqualTo(1000.0);
    }
}
