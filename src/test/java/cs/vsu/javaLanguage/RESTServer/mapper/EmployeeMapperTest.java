package cs.vsu.javaLanguage.RESTServer.mapper;

import cs.vsu.javaLanguage.RESTServer.dto.EmployeeDTO;
import cs.vsu.javaLanguage.RESTServer.mapper.EmployeeMapper;
import cs.vsu.javaLanguage.RESTServer.postgres.model.EmployeeSalary;
import cs.vsu.javaLanguage.RESTServer.redis.model.Employee;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMapperTest {

    private final EmployeeMapper employeeMapper = new EmployeeMapper();

    @Test
    void testToDTO_EmployeeSalary() {
        EmployeeSalary employeeSalary = new EmployeeSalary(1, 1000.0, LocalDate.of(2023, 1, 1));
        EmployeeDTO employeeDTO = employeeMapper.toDTO(employeeSalary);
        assertEquals("1", employeeDTO.getEmployeeId());
        assertEquals(1000.0, employeeDTO.getSalary());
        assertEquals(LocalDate.of(2023, 1, 1), employeeDTO.getDate());
    }

    @Test
    void testToDTO_Employee() {
        Employee employee = new Employee("1", 1000.0);
        EmployeeDTO employeeDTO = employeeMapper.toDTO(employee);
        assertEquals("1", employeeDTO.getEmployeeId());
        assertEquals(1000.0, employeeDTO.getSalary());
        assertEquals(LocalDate.now(), employeeDTO.getDate());
    }
}
