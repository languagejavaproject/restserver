package cs.vsu.javaLanguage.RESTServer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cs.vsu.javaLanguage.RESTServer.dto.EmployeeDTO;
import cs.vsu.javaLanguage.RESTServer.service.EmployeeSalaryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmployeeSalaryController.class)
public class EmployeeSalaryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private EmployeeSalaryService employeeSalaryService;

    @Test
    public void getEmployeeSalary_returnsCorrectSalary() throws Exception {
        EmployeeDTO request = EmployeeDTO.builder()
                .employeeId("123")
                .date(LocalDate.of(2023, 2, 1))
                .build();
        EmployeeDTO response = EmployeeDTO.builder()
                .employeeId("123")
                .date(LocalDate.of(2023, 2, 1))
                .salary(5000.0)
                .build();

        when(employeeSalaryService.findSalaryByDate(request)).thenReturn(response);

        String jsonRequest = objectMapper.writeValueAsString(request);
        String jsonResponse = objectMapper.writeValueAsString(response);

        mockMvc.perform(post("/api/v1/employee/salary")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonRequest))
                .andExpect(status().isOk())
                .andExpect(content().json(jsonResponse));
    }

}
