package cs.vsu.javaLanguage.RESTServer.postgres.repository;

import cs.vsu.javaLanguage.RESTServer.postgres.model.EmployeeSalary;
import cs.vsu.javaLanguage.RESTServer.postgres.repository.EmployeeSalaryPostgresRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
class EmployeeSalaryPostgresRepositoryUnitTest {

    @Autowired
    private EmployeeSalaryPostgresRepository employeeSalaryPostgresRepository;

    @AfterEach
    void tearDown() {
        employeeSalaryPostgresRepository.deleteAll();
    }

    @Test
    void testSaveEmployeeSalary() {
        EmployeeSalary employeeSalary = EmployeeSalary.builder()
                .salary(1000)
                .date(LocalDate.of(2023, 1, 1))
                .build();

        EmployeeSalary savedEmployeeSalary = employeeSalaryPostgresRepository.save(employeeSalary);

        assertThat(savedEmployeeSalary).isNotNull();
        assertThat(savedEmployeeSalary.getSalary()).isEqualTo(1000.0);
        assertThat(savedEmployeeSalary.getDate()).isEqualTo(LocalDate.of(2023, 1, 1));
    }

    @Test
    void testFindAllEmployeeSalaries() {
        EmployeeSalary employeeSalary1 = EmployeeSalary.builder()
                .salary(1000)
                .date(LocalDate.of(2023, 1, 1))
                .build();

        EmployeeSalary employeeSalary2 = EmployeeSalary.builder()
                .salary(2000)
                .date(LocalDate.of(2023, 1, 1))
                .build();

        employeeSalaryPostgresRepository.save(employeeSalary1);
        employeeSalaryPostgresRepository.save(employeeSalary2);

        List<EmployeeSalary> employeeSalaries = employeeSalaryPostgresRepository.findAll();

        assertThat(employeeSalaries).hasSize(2);
        assertThat(employeeSalaries).contains(employeeSalary1, employeeSalary2);
    }

    @Test
    void testUpdateEmployeeSalary() {
        EmployeeSalary employeeSalary = EmployeeSalary.builder()
                .salary(1000)
                .date(LocalDate.of(2023, 1, 1))
                .build();

        EmployeeSalary savedEmployeeSalary = employeeSalaryPostgresRepository.save(employeeSalary);

        savedEmployeeSalary.setSalary(2000.0);

        EmployeeSalary updatedEmployeeSalary = employeeSalaryPostgresRepository.save(savedEmployeeSalary);

        assertThat(updatedEmployeeSalary).isNotNull();
        assertThat(updatedEmployeeSalary.getSalary()).isEqualTo(2000.0);
        assertThat(updatedEmployeeSalary.getDate()).isEqualTo(LocalDate.of(2023, 1, 1));
    }

    @Test
    void testDeleteEmployeeSalary() {
        EmployeeSalary employeeSalary = EmployeeSalary.builder()
                .salary(1000)
                .date(LocalDate.of(2023, 1, 1))
                .build();

        employeeSalaryPostgresRepository.save(employeeSalary);

        employeeSalaryPostgresRepository.deleteById((int) employeeSalary.getEmployeeId());

        Optional<EmployeeSalary> deletedEmployeeSalary = employeeSalaryPostgresRepository.findById((int) employeeSalary.getEmployeeId());

        assertThat(deletedEmployeeSalary).isEmpty();
    }

    @Test
    void testFindByEmployeeIdAndDate() {
        EmployeeSalary employeeSalary = EmployeeSalary.builder()
                .salary(1000)
                .date(LocalDate.of(2023, 1, 1))
                .build();

        EmployeeSalary savedEmployeeSalary = employeeSalaryPostgresRepository.save(employeeSalary);

        Long generatedEmployeeId = savedEmployeeSalary.getEmployeeId();

        Optional<EmployeeSalary> foundEmployeeSalary = employeeSalaryPostgresRepository.findByEmployeeIdAndDate(generatedEmployeeId, LocalDate.of(2023, 1, 1));

        assertThat(foundEmployeeSalary).isPresent();
        assertThat(foundEmployeeSalary.get().getEmployeeId()).isEqualTo(generatedEmployeeId);
        assertThat(foundEmployeeSalary.get().getSalary()).isEqualTo(1000.0);
        assertThat(foundEmployeeSalary.get().getDate()).isEqualTo(LocalDate.of(2023, 1, 1));
    }

}
